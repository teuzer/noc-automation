// ==UserScript==
// @name         Bar Axon
// @version      2.2
// @downloadURL  https://bitbucket.org/teuzer/noc-automation/raw/master/baraxon.user.js
// @updateURL    https://bitbucket.org/teuzer/noc-automation/raw/master/baraxon.user.js
// @description  Barra auxiliar como atalho para caminhos principais
// @author       Teuzer
// @match        http://10.125.18.66:8080/Axon4/*
// @grant        none
// ==/UserScript==

(function() {
    let MouseBehavior = (item)=>{
        item.addEventListener("mouseenter",()=>{
            item.style.background = "#2d425d";
        },false);
        item.addEventListener("mouseleave",()=>{
            item.style.background = "#5c88c1";
        },false);
    }
    let controlPanel = document.createElement("div");
    controlPanel.innerText = "Painel de Controle";
    controlPanel.style.position = "relative";
    controlPanel.style.textAlign = "center";
    controlPanel.style.background = "#5c88c1";
    controlPanel.style.marginLeft = "10px";
    controlPanel.style.height = "100%";
    controlPanel.style.float = "left";
    controlPanel.style.border = "1px solid black";
    controlPanel.style.borderRadius = "3px";
    controlPanel.style.padding = "1px 10px";
    controlPanel.style.color = "white";
    controlPanel.style.fontFamily = "calibri";
    controlPanel.style.cursor = "pointer";
    controlPanel.addEventListener("click",()=>{
        window.location.href = "http://10.125.18.66:8080/Axon4/event/controlPanelAction.seam";
    },false);
    MouseBehavior(controlPanel);
    let searchUnit = document.createElement("div");
    searchUnit.innerText = "Pesquisa de Unidades Consumidoras";
    searchUnit.style.position = "relative";
    searchUnit.style.textAlign = "center";
    searchUnit.style.background = "#5c88c1";
    searchUnit.style.marginLeft = "10px";
    searchUnit.style.height = "100%";
    searchUnit.style.float = "left";
    searchUnit.style.border = "1px solid black";
    searchUnit.style.borderRadius = "3px";
    searchUnit.style.padding = "1px 10px";
    searchUnit.style.color = "white";
    searchUnit.style.fontFamily = "calibri";
    searchUnit.style.cursor = "pointer";
    searchUnit.addEventListener("click",()=>{
        window.location.href = "http://10.125.18.66:8080/Axon4/client/clientList.seam";
    },false);
    MouseBehavior(searchUnit);
    setTimeout(()=>{
        document.querySelector("table[class^='rich'] > tbody > tr > td:nth-child(1)").appendChild(controlPanel);
        document.querySelector("table[class^='rich'] > tbody > tr > td:nth-child(1)").appendChild(searchUnit);

    },1000);
    if(window.location.href.toUpperCase().includes("CLIENTLIST")){
            let trStrategy = document.createElement('tr');
            let tdStrategyOne = document.createElement('td');
            let tdStrategyTwo = document.createElement('td');
            let tdStrategyThree = document.createElement('td');
            let btnStrategyOne = document.createElement('div');
            btnStrategyOne.innerText= document.querySelector("option[value='SWITCH_COMM_PORT']").innerText;
            btnStrategyOne.style.position= "relative";
            btnStrategyOne.style.borderRadius= "3px";
            btnStrategyOne.style.fontSize= "16px";
            btnStrategyOne.style.fontFamily= "calibri";
            btnStrategyOne.style.fontWeight= "normal";
            btnStrategyOne.style.padding= "0px 18px";
            btnStrategyOne.style.boxSizing= "border-box";
            btnStrategyOne.style.border = "1px solid black";
            btnStrategyOne.style.background = "rgb(92, 136, 193)";
            btnStrategyOne.style.cursor = "pointer";
            btnStrategyOne.style.color = "white";
            let btnStrategyTwo = btnStrategyOne.cloneNode(false);
            let btnStrategyThree = btnStrategyOne.cloneNode(false);
            btnStrategyOne.addEventListener("click",function(){
                document.querySelector("#searchClients\\:searchStrategySelect").value = "SWITCH_COMM_PORT";
            },false);
            MouseBehavior(btnStrategyOne);
            btnStrategyTwo.innerText= document.querySelector("option[value='CONSUMER_UNIT']").innerText;
            btnStrategyTwo.addEventListener("click",function(){
                document.querySelector("#searchClients\\:searchStrategySelect").value = "CONSUMER_UNIT";
            },false);
            MouseBehavior(btnStrategyTwo);
            btnStrategyThree.innerText= document.querySelector("option[value='MCI']").innerText;
            btnStrategyThree.addEventListener("click",function(){
                document.querySelector("#searchClients\\:searchStrategySelect").value = "MCI";
            },false);
            MouseBehavior(btnStrategyThree);
            trStrategy.append(tdStrategyOne);
            trStrategy.append(tdStrategyTwo);
            trStrategy.append(tdStrategyThree);
            tdStrategyOne.append(btnStrategyOne);
            tdStrategyTwo.append(btnStrategyTwo);
            tdStrategyThree.append(btnStrategyThree);
             document.querySelector("body > table > tbody > tr:nth-child(3) > td > table > tbody > tr > td label").innerText = "Unidade Consumidora";
            let panelStrategyWrapper = document.querySelector("body > table > tbody > tr:nth-child(3) > td > table > tbody");
            panelStrategyWrapper.style.fontFamily = "calibri";
            panelStrategyWrapper.querySelector("tr:nth-child(1)").after("Pesquisar por:");
            panelStrategyWrapper.insertBefore(trStrategy, panelStrategyWrapper.querySelector("tr:nth-child(2)"));
        }else if(window.location.href.toUpperCase().includes("CLIENTHOME")){
            let copyPreviousFct = function(ev){
                let curElem = ev.target;
                curElem.previousElementSibling.select();
                document.execCommand("copy");
                curElem.previousElementSibling.blur();
                curElem.innerText = "Copiado!";
                setTimeout(function(){
                    curElem.innerText = "Copiar";
                },500);
            }
            let btnCopy = document.createElement('div');
            btnCopy.innerText= "Copiar";
            btnCopy.style.position= "relative";
            btnCopy.style.float= "right";
            btnCopy.style.borderRadius= "3px";
            btnCopy.style.fontSize= "16px";
            btnCopy.style.fontFamily= "calibri";
            btnCopy.style.fontWeight= "normal";
            btnCopy.style.textAlign= "center";
            btnCopy.style.padding= "3px 12px";
            btnCopy.style.boxSizing= "border-box";
            btnCopy.style.border = "1px solid black";
            btnCopy.style.background = "rgb(92, 136, 193)";
            btnCopy.style.cursor = "pointer";
            btnCopy.style.color = "white";
            MouseBehavior(btnCopy);
            btnCopy.addEventListener("click",copyPreviousFct,false);
            let btnCopyTwo = btnCopy.cloneNode(false);
            MouseBehavior(btnCopyTwo);
            btnCopyTwo.innerText= "Copiar";
            btnCopyTwo.addEventListener("click",copyPreviousFct,false);
            let equipName = document.querySelector("label.name[for*='mccField']").nextElementSibling.append(btnCopy);
            let equipDetail = document.querySelector("label.name[for*='detailField']").nextElementSibling.append(btnCopyTwo);
        }
})();