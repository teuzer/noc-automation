// ==UserScript==
// @name         OSTicket Automatic Comment
// @version      2.1
// @downloadURL  https://bitbucket.org/teuzer/noc-automation/raw/master/OSticketAutoComment.user.js
// @updateURL    https://bitbucket.org/teuzer/noc-automation/raw/master/OSticketAutoComment.user.js
// @description  Script para comentar tickets automaticamente.
// @author       Teuzer
// @match        http://10.154.77.34/osticket/scp/tickets.php*
// @match        http://e6aaziosta00/osticket/scp/tickets.php*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    let doAutomation = ()=>{
        let TicketToComment = JSON.parse(localStorage.getItem("TicketToComment"));
        let DataToComment = JSON.parse(localStorage.getItem("DataToComment"));
        let TicketToComStatus = JSON.parse(localStorage.getItem("TicketToComStatus"));
        let CurItem = parseInt(JSON.parse(localStorage.getItem("CurItem")));

        let nextStepFunction = ()=>{
            TicketToComStatus[CurItem] = "COMENTADO";
            localStorage.setItem("TicketToComStatus",JSON.stringify(TicketToComStatus));
            CurItem = CurItem + 1;
            localStorage.setItem("CurItem",parseInt(CurItem));
            if((CurItem < TicketToComment.length)){
                window.location.href = window.location.origin+"/osticket/scp/tickets.php?a=search&search-type=&query="+TicketToComment[CurItem];
            }else{
                window.location.href = window.location.origin+"/osticket/scp/tickets.php?a=search&search-type=&query="+TicketToComment[CurItem - 1];
            }
        };

        let commentOnTicket = ()=>{
            let event = new Event('change');
            document.querySelector("#resp_sec > tr:nth-child(2) > td:nth-child(2) > div:nth-child(2) > div > div.redactor-editor.redactor-linebreaks").innerHTML = DataToComment[CurItem].replace("§"," ");
            document.querySelector("textarea#response").innerHTML = DataToComment[CurItem].replace("§"," ");
            document.querySelector("textarea#response").value = DataToComment[CurItem].replace("§"," ");
            setTimeout(()=>{
                document.querySelector("#resp_sec > tr:nth-child(2) > td:nth-child(2) > div:nth-child(2) > div > div.redactor-editor.redactor-linebreaks").parentNode.dispatchEvent(event);
                document.querySelector("#reply > p > input.save.pending").click();
            },500);
        };

        let searchTicketById = ()=>{
            if(document.querySelector("#tickets > table > tbody").children.length > 0){
                let nextLocation = window.location.origin+"/osticket/scp/tickets.php?a=search&search-type=&query="+TicketToComment[CurItem];
                document.querySelectorAll("#tickets > table > tbody > tr > td:nth-child(2)").forEach((elem)=>{
                    if(elem.innerText.toUpperCase() == TicketToComment[CurItem]){
                        nextLocation = window.location.origin+elem.querySelector("a").getAttribute("href");
                    }
                });
                window.location.href = nextLocation;
            }
        };
        let checkAlreadyCommented = ()=>{
            let AlCom = false;
            document.querySelectorAll("div[id*='thread-entry']").forEach((elem)=>{
                let c = new Date(elem.querySelector("time").getAttribute("datetime"));
                let n = new Date();
                if((n - c)/1000 < 100){
                    if(elem.querySelector("div.thread-body").innerText.toUpperCase().startsWith(DataToComment[CurItem].toUpperCase().split(" ")[0])){
                        AlCom = true;
                    }
                }
            });
            if(!AlCom){
                commentOnTicket();
            }else{
                nextStepFunction();
            }
        };
        let checkUrl = ()=>{
            if(window.location.href.includes("tickets.php?id=")){
                document.querySelector("body > auto-comment").shadowRoot.querySelector("#\\#mine-auto-comment").style.display = "none";
                setTimeout(()=>{
                    if((TicketToComment.includes(document.querySelector("h2").innerText.split("#")[1].replace(/ /g,""))) &&
                        (parseInt(TicketToComment[CurItem])==parseInt(document.querySelector("h2").innerText.split("#")[1].replace(/ /g,"")))){
                        checkAlreadyCommented();
                    }else{
                        window.location.href = window.location.origin+"/osticket/scp/tickets.php?a=search&search-type=&query="+TicketToComment[CurItem];
                    }
                },2000);
            }else if(window.location.href.includes("tickets.php?a=search&search-type=&query=")){
                setTimeout(()=>{searchTicketById();},1000);
            }
        };
        setTimeout(()=>{checkUrl();},1000);
    };
    class autoComment extends HTMLElement {
        constructor() {
            super();

            const shadow = this.attachShadow({mode: 'open'});

            const wrapper = document.createElement('div');
            wrapper.setAttribute('class', 'w3-col s12 w3-light-grey w3-topbar');

            const W3CSS = document.createElement('link');
            W3CSS.setAttribute('rel', 'stylesheet');
            W3CSS.setAttribute('href', 'https://www.w3schools.com/w3css/4/w3.css');

            const wrapperHeader = document.createElement('div');
            wrapperHeader.setAttribute('class', 'w3-col s12 w3-padding w3-light-grey w3-bottombar');

            const wrapperBody = document.createElement('div');
            wrapperBody.setAttribute('class', 'w3-col s12 w3-light-grey w3-bottombar');

            const modalToggler = document.createElement('div');
            modalToggler.style.position = "fixed";
            modalToggler.style.top = "0";
            modalToggler.style.left = "calc(50vw - 490px)";
            modalToggler.style.width = "100vw";
            modalToggler.style.height = "100vh";
            modalToggler.style.background = "rgba(0,0,0,.5)";
            modalToggler.style.display = "none";

            shadow.appendChild(W3CSS);
            shadow.appendChild(wrapper);
            wrapper.appendChild(modalToggler);
            wrapper.appendChild(wrapperHeader);
            wrapper.appendChild(wrapperBody);

            if (!localStorage.TicketToComment) {
                localStorage.setItem("TicketToComment",JSON.stringify(null));
            }
            if (!localStorage.DataToComment) {
                localStorage.setItem("DataToComment",JSON.stringify(null));
            }
            if (!localStorage.TicketToComStatus) {
                localStorage.setItem("TicketToComStatus",JSON.stringify(null));
            }
            if (!localStorage.isAutoStarted) {
                localStorage.setItem("isAutoStarted",JSON.stringify(false));
            }
            if (!localStorage.CurItem) {
                localStorage.setItem("CurItem",Number(0));
            }

            const developedBy = document.createElement("div");
            developedBy.innerText = "Desenvolvido por Matheus Augusto - BR0034864741";
            developedBy.style.position = "fixed";
            developedBy.style.width = "calc(100vw - 490px)";
            developedBy.style.left = "calc(50vw - 490px)";
            developedBy.style.bottom = "0px";
            developedBy.setAttribute('class', 'w3-padding w3-col w3-light-grey w3-topbar w3-rightbar w3-center');

            let TicketToComment = JSON.parse(localStorage.getItem("TicketToComment"));
            let DataToComment = JSON.parse(localStorage.getItem("DataToComment"));
            let TicketToComStatus = JSON.parse(localStorage.getItem("TicketToComStatus"));
            let CurItem = parseInt(JSON.parse(localStorage.getItem("CurItem")));
            if((TicketToComment != null) && (DataToComment != null)){
                if((TicketToComment.length > 0) && (DataToComment.length > 0) && (TicketToComment.length == DataToComment.length)){
                    let initiateAutomation = document.createElement('div');
                    initiateAutomation.setAttribute('class','w3-btn w3-margin-bottom w3-padding w3-round');
                    initiateAutomation.setAttribute('id','#mine-auto-comment');
                    initiateAutomation.style.float = "none";
                    initiateAutomation.style.margin = "auto";
                    initiateAutomation.style.display = "table";

                    if(JSON.parse(localStorage.getItem("isAutoStarted")) && (CurItem < TicketToComment.length)){
                        initiateAutomation.classList.add("w3-red");
                        initiateAutomation.innerHTML = "Pausar Automação";
                        modalToggler.style.display= "table";
                        doAutomation();
                    }else{
                        initiateAutomation.classList.add("w3-blue");
                        initiateAutomation.innerText = "Iniciar Automação";
                        modalToggler.style.display= "none";
                        localStorage.isAutoStarted = false;
                    }
                    initiateAutomation.addEventListener('click',()=>{
                        if(JSON.parse(localStorage.getItem("isAutoStarted"))){
                            initiateAutomation.innerHTML = "Iniciar Automação";
                            modalToggler.style.display= "none";
                            localStorage.isAutoStarted = false;
                            initiateAutomation.classList.remove("w3-red");
                            initiateAutomation.classList.add("w3-blue");
                        }else{
                            if(CurItem < TicketToComment.length){
                                initiateAutomation.innerHTML = "Pausar Automação";
                                localStorage.isAutoStarted = true;
                                initiateAutomation.classList.remove("w3-blue");
                                initiateAutomation.classList.add("w3-red");
                                modalToggler.style.display= "table";
                                window.location.href = window.location.origin+"/osticket/scp/tickets.php?a=search&search-type=&query="+TicketToComment[CurItem];
                            }else{
                                alert("Automação Completa.");
                            }
                        }
                    },false);
                    wrapperHeader.appendChild(initiateAutomation);

                    let tableDataWrapper = document.createElement('div');
                    tableDataWrapper.style.overflow = "auto";
                    tableDataWrapper.style.height = "88vh";

                    let tableData = document.createElement('table');
                    tableData.setAttribute('class','w3-table-all');
                    let tableDataHeader = document.createElement('thead');
                    let tableDataBody = document.createElement('tbody');
                    let tableDataTh = document.createElement('th');
                    let tableDataTr = document.createElement('tr');
                    let tableDataTrHead = document.createElement('tr');
                    tableDataTrHead.setAttribute('class','w3-blue');
                    let tableDataTd = document.createElement('td');
                    ["ID","TICKET","STATUS"].forEach((elem,index)=>{
                        let th = tableDataTh.cloneNode(false);
                        th.innerText = elem;
                        th.setAttribute('class','w3-border')
                        tableDataTrHead.append(th);
                    });

                    tableDataWrapper.append(tableData);
                    tableData.append(tableDataHeader);
                    tableDataHeader.append(tableDataTrHead);
                    tableData.append(tableDataBody);
                    
                    TicketToComment.forEach((elem,index)=>{
                        let currentTr = tableDataTr.cloneNode(false);
                        let tdText = [index + 1, elem,TicketToComStatus[index]];
                        tdText.forEach((el)=>{
                            let td = tableDataTd.cloneNode(false);
                            td.innerText = el;
                            td.setAttribute('class','w3-border')
                            currentTr.append(td);
                        });
                        tableDataBody.append(currentTr);
                    });

                    wrapperBody.appendChild(tableDataWrapper);

                    const clearButton = document.createElement("button");
                    clearButton.innerText = "Limpar Dados";
                    clearButton.style.position = "absolute";
                    clearButton.style.left = "0px";
                    clearButton.style.bottom = "0";
                    clearButton.setAttribute('class', 'w3-btn w3-col s12 w3-red w3-hover-dark-grey');
                    clearButton.setAttribute('id', '#mine-cleaner');
                    clearButton.addEventListener("click",()=>{
                        localStorage.setItem("TicketToComment",JSON.stringify(null));
                        localStorage.setItem("DataToComment",JSON.stringify(null));
                        localStorage.setItem("TicketToComStatus",JSON.stringify(null));
                        localStorage.setItem("isAutoStarted",JSON.stringify(false));
                        localStorage.setItem("CurItem",Number(0));
                        window.location.reload();
                    },false);
                    wrapperBody.appendChild(clearButton);
                }else{
                    Alert("Dados inconsistentes. Tente novamente.");
                    localStorage.setItem("TicketToComment",JSON.stringify(null));
                    localStorage.setItem("DataToComment",JSON.stringify(null));
                    localStorage.setItem("TicketToComStatus",JSON.stringify(null));
                    localStorage.setItem("isAutoStarted",JSON.stringify(false));
                    localStorage.setItem("CurItem",Number(0));
                }
            }else{
                const sheetGuide = document.createElement("a");
                sheetGuide.href = "https://drive.google.com/file/d/1hpzswv_IMTngpIPajqN1e16AH_lqFzTx/view?usp=drivesdk";
                sheetGuide.setAttribute('target', 'blank');
                sheetGuide.setAttribute('class', 'w3-btn w3-col s12 w3-green w3-hover-teal w3-round w3-margin-bottom');
                sheetGuide.innerText = "Baixar planilha guia"

                const textarea = document.createElement("textarea");
                textarea.setAttribute('class', 'w3-col s12');
                textarea.setAttribute('placeholder', 'Insira os dados aqui.');
                textarea.style.position = "relative";
                textarea.style.height = "80vh";
                textarea.style.padding = "0";
                textarea.style.border = "none";
                textarea.style.resize = "none";
                textarea.setAttribute('id', '#mine-items');

                const populateButton = document.createElement("button");
                populateButton.setAttribute('class', 'w3-btn w3-col s12 w3-blue w3-hover-indigo');
                populateButton.innerText = "Inserir dados";
                populateButton.style.fontSize= "16px";
                populateButton.setAttribute('id', '#mine-populate');
                populateButton.addEventListener("click",()=>{
                    let test = textarea.value.split(/\r?\n/g);
                    textarea.value.split(/\r?\n/g);
                    test = test.filter((elem)=>{
                        if((elem.trim().length > 0) && /\t/g.test(elem.trim())){
                            return elem;
                        }
                    });
                    if(test.length > 0){
                        let TicketToComment = [];
                        let DataToComment = [];
                        let TicketToComStatus = [];
                        test.forEach((elem)=>{
                            if(elem.split(/\t/g).length == 2){
                                let elama = elem.split(/\t/g);
                                TicketToComment.push(elama[0]);
                                DataToComment.push(elama[1]);
                                TicketToComStatus.push("AGUARDANDO");
                            }
                        });
                        if((TicketToComment.length == DataToComment.length)&&(DataToComment.length == TicketToComStatus.length)){
                            localStorage.setItem("TicketToComment",JSON.stringify(TicketToComment));
                            localStorage.setItem("DataToComment",JSON.stringify(DataToComment));
                            localStorage.setItem("TicketToComStatus",JSON.stringify(TicketToComStatus));
                            localStorage.setItem("isAutoStarted",JSON.stringify(false));
                            localStorage.setItem("CurItem",Number(0));
                            alert("Lista Populada com sucesso!");
                            window.location.href = window.location.origin+"/osticket/scp/tickets.php";
                        }else{
                            alert("Erro ao inserir dados. Por favor, tente novamente.");
                            window.location.href = window.location.origin+"/osticket/scp/tickets.php";
                        }
                    }else{
                        alert("Nenhum equipamento colocado!");
                    }
                },false);
                wrapperHeader.appendChild(sheetGuide);
                wrapperHeader.innerHTML += "Cole os dados na área em branco abaixo.";
                wrapperBody.appendChild(textarea);
                wrapperBody.appendChild(populateButton);
            }
            wrapperBody.appendChild(developedBy);
        }
    }

    customElements.define('auto-comment', autoComment);
    let shadow = document.createElement('auto-comment');
    shadow.style.position = "fixed";
    shadow.style.zIndex = "99";
    shadow.style.left = "0";
    shadow.style.top = "0";
    shadow.style.width = "calc(50vw - 490px)";
    shadow.style.minHeight = "100vh";
    shadow.style.overflow = "hidden";
    shadow.style.borderRight = "5px solid #ccc";
    shadow.style.boxSizing = "border-box";
    shadow.style.background = "#eeeeee";
    document.querySelector("body").appendChild(shadow);
})();