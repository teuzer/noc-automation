// ==UserScript==
// @name         Arto Painel Info
// @version      2.1
// @downloadURL  https://bitbucket.org/teuzer/noc-automation/raw/master/artoPanelInfo.user.js
// @updateURL    https://bitbucket.org/teuzer/noc-automation/raw/master/artoPanelInfo.user.js
// @description  Adiciona funcionalidades de informações no arto do painel
// @author       Teuzer | BR0034864741
// @match        http://10.125.18.64/arto/*
// @connect      10.154.77.34
// @connect      portalcos
// @grant        GM_xmlhttpRequest
// @grant        GM_openInTab
// ==/UserScript==
$(function(){
    let denyStorage = true;
    if (typeof(Storage) !== "undefined") {
        denyStorage = false;
        let updateDisponibily = ()=>{
            let numDaysLastMonth = new Date(new Date().getFullYear(), new Date().getMonth(), 0).getDate();
            let [dateLast ,monthLast , yearLast] = (new Date(new Date().setDate(new Date().getDate()-(1+numDaysLastMonth)))).toLocaleDateString().split("/");
            let [date ,month, year] = (new Date(new Date().setDate(new Date().getDate()-1))).toLocaleDateString().split("/");
            let myData = new FormData();
            myData.append("Acao", "ListarCanais");
            myData.append("DataInicio", dateLast+"/"+String(monthLast).padStart(2, '0')+"/"+yearLast);
            myData.append("DataFim", date+"/"+String(month).padStart(2, '0')+"/"+year);
            myData.append("Ordem", "1");
            myData.append("Asc", "asc");
            GM_xmlhttpRequest({
                method: "POST",
                url: "http://portalcos//canais_cod//index.php",
                data: myData,
                onload : function(res){
                    localStorage.lastUpdate = Math.floor(Date.now() / (60*60*100*24));
                    let availability = {};
                    let totalEquip = {};
                    let doc = new DOMParser();
                    doc = doc.parseFromString(res.responseText, "text/html");
                    doc.querySelectorAll("table tr > td:nth-child(1)").forEach((elem,index)=>{
                        let dados = elem.parentNode.querySelectorAll("td");
                        totalEquip[index] = dados[0].innerText;
                        availability[index] = dados[3].innerText;
                    });
                    localStorage.setItem('availability', JSON.stringify(availability));
                    localStorage.setItem("totalEquip",JSON.stringify(totalEquip));
                    localStorage.lastUpdate = Math.floor(Date.now() / (60*60*100*24));
                }
            });
        };
        if (!localStorage.lastUpdate) {
            if (!localStorage.availability) {
                localStorage.availability = JSON.stringify({});
            }
            if (!localStorage.totalEquip) {
                localStorage.totalEquip = JSON.stringify({});
            }
            updateDisponibily();
        }else{
            let today = Math.floor(Date.now() / (60*60*100*24));
            let lastUpdate = JSON.parse(localStorage.getItem("lastUpdate"));
            if(Math.abs((today - lastUpdate)) >= 1){
                updateDisponibily();
            }
        }
    }
    let instanse = false;
    let ANAPOLIS = ['AA','ABA','ABA_1','ABA_2','ALE','ALE_1','ALE_2','ALE_3','ALE_31','ALE_33','ALE_34','ALX','ALX_1','ALX_2','ALX_3','AN','ANA','ANA_1','ANA_10','ANA_11','ANA_12','ANA_2','ANA_3','ANA_32','ANA_4','ANA_5','ANA_6','ANA_7','ANA_9','AX','BA','BAL','BAL_1','BAL_2','BAL_34','CB','CBR','CBR_1','CBR_2','CBR_32','CCZ','CCZ_1','CCZ_2','CLG','CLG_1','CLG_2','CRB','CRB_1','CRB_2','CRB_30','CRB_32','DAI','DAI_1','DAI_2','DAI_3','DAI_31','DAI_4','DAI_5','DAI_6','DAI_7','DAI_8','DAI_9','DCP','DF','EMB_30','FCC','GAP','GAP_1','GAP_2','GE','GNS','GNS_1','GNS_2','GNS_3','GNS_32','GNS_33','GP','GRS','GRS_1','GRS_2','JA','JAR','JAR_1','JAR_2','JE','JES','JRP','JRP_1','JUC','JUC_2','JUN','JUN_1','JUN_2','JUN_3','JUN_4','JUN_5','JUN_6','JUN_7','JUN_8','MBS','MTG','MTG_1','MTG_2','MTG_3','NP','ODA','ODA_1','OV','OVG','OVG_1','PET','PET_1','PET_2','PG','PRP','PRP_1','PRP_2','PRP_3','PRP_4','PRS','PV','RIA','SAN','SAN_2','SF','SFG','SFG_1','SFG_2','SFG_31','SFG_32','SOZ','SOZ_1','SOZ_2','SR','SRO','SRO_1','SRO_2','TEZ','TEZ_1','TEZ_2','TJ','UAG','UAG_2','UTG','VPI','VPI_1','VPI_31','ZN',];
    let FORMOSA = ['ALP','ALP_1','AQ','AVN','AVN_1','AVN_31','AVN_33','BBS','BBS_1','BF','BQ','BSC','BSC_1','CAB','CAB_1','CAB_2','CAB_33','CBE','CBE_1','CH','CJ','CW','DA','DPA','FA','FB','FLO','FLO_31','FLO_32','GH','GY','IA','IAC','IAC_1','IAC_31','IAC_32','IAC_33','ITQ','ITQ_1','ITQ_2','ITQ_3','ITQ_31','ITQ_33','ITQ_34','ITQ_35','JL','MA','MA_33','MAB','MAB_1','MAB_2','MAL','MB','MB2','MB2_1','ND','PI','PLG','PLG_1','PLG_3','PLG_31','PLG_32','PLT','PLT_1','PLT_2','PLT_3','POS','POS_1','POS_2','PRN','PRN_2','PS','SA','SAB','SAB_1','SD','SGA','SI','SJA','SJA_1','SJA_31','SJA_32','SJA_33','SJA_34','SRN','SRN_1','TE','TI','TL','UMA','USD','USD_1','USD_31','USD_32','UTA','VBO',];
    let GOIANIA = ['AER','AER_1','AER_10','AER_11','AER_12','AER_13','AER_14','AER_15','AER_16','AER_17','AER_18','AER_2','AER_3','AER_4','AER_5','AER_6','AER_7','AER_8','AER_9','ARS','ATL','ATL_1','ATL_10','ATL_11','ATL_12','ATL_13','ATL_14','ATL_15','ATL_2','ATL_3','ATL_4','ATL_5','ATL_6','ATL_7','ATL_8','ATL_9','BR','CAM','CAM_1','CAM_10','CAM_11','CAM_12','CAM_13','CAM_14','CAM_2','CAM_3','CAM_4','CAM_5','CAM_7','CAM_8','CAM_9','CJA','CJA_1','CJA_10','CJA_11','CJA_12','CJA_2','CJA_3','CJA_4','CJA_5','CJA_6','CJA_7','CJA_8','CJA_9','COS','FER','FER_1','FER_10','FER_13','FER_14','FER_15','FER_16','FER_18','FER_2','FER_3','FER_4','FER_5','FER_6','FER_7','FER_8','FER_9','GN','GOI','GOI_1','GOI_10','GOI_11','GOI_12','GOI_2','GOI_3','GOI_4','GOI_5','GOI_6','GOI_7','GOI_8','GOI_9','GOL','GOL_1','GOL_10','GOL_11','GOL_12','GOL_13','GOL_14','GOL_15','GOL_16','GOL_17','GOL_19','GOL_2','GOL_20','GOL_21','GOL_22','GOL_23','GOL_3','GOL_4','GOL_5','GOL_6','GOL_7','GOL_8','GOL_9','MD1','MPO','MPO_2','MPO_3','MPO_4','MPO_5','MPO_6','REL_1','REL_10','REL_11','REL_2','REL_3','REL_4','REL_5','REL_6','REL_7','REL_8','REL_9','RT','XAV','XAV_1','XAV_2','XAV_3','XAV_4','XIV',];
    let IPORA = ['AC','ADE','ADE_1','ADE_2','AG','AI','AK','AM','AMB','AMB_1','AMB_2','AMO','AMO_1','ANI','ANI_1','ANI_2','ANI_30','ANI_31','ANI_32','ARA','ARA_1','ARA_2','ARA_3','ARE','ARE_1','ARE_2','ARE_31','ARI','ARN','ARN_1','ARP','ARP_1','ARU','ARU_1','ARU_2','AUR','AUR_1','AUR_32','AVE','AVE_1','AVE_2','AY','AZ','BCL','BDG','BDG_1','BER','BGA','BGA_1','BH','BJA','BJA_1','BLZ','BLZ_1','BM','BRI','BRI_31','BRI_33','BRT','BRT_1','BRT_2','BRT_31','BS','BT','CAI','CAI_1','CAI_2','CAI_32','CED','CEZ','CEZ_1','CEZ_2','CEZ_30','CG','CLD','CO','COO','COO_1','CP','CZ','DI','DIO','DIO_1','DN','DOV','DOV_1','DV','FAI','FAI_1','FAI_2','FC','FCA','FCA_3','FCA_31','FCA_32','FIR','FIR_1','FIR_3','FIR_4','FN','FNO','FNO_1','FNO_2','FR','GK','GOA','GOA_1','GOA_2','GOA_31','GOA_32','GS','HE','HEI','HEI_1','HEI_2','IB','IC','II','IK','IND','IND_1','IND_2','IPO','IPO_1','IPO_2','IPO_3','IPO_31','IPO_32','IPU','IPU_1','IPU_2','IPU_3','IPU_4','IPU_5','IR','IS','ISR','ISR_2','ISR_30','ISR_32','ITA','ITA_1','ITA_2','ITA_3','ITA_30','ITA_31','ITA_32','ITA_33','ITA_34','ITA_4','ITG','ITG_1','ITG_2','ITG_3','ITG_31','ITG_4','ITG_5','ITI','ITI_1','ITI_2','ITR','ITR_1','ITR_2','ITU','ITU_2','IU','IV','IY','JAN','JAN_1','JAN_2','JAU','JAU_1','JCL','JCL_2','JD','JP','JR','JUR','JUR_1','JUS','JUS_1','JUS_2','JUS_32','JUS_33','KB','MAG','MAG_1','MAT','MAT_31','MCG','MCG_1','MD','ME','MO','MOD','MOD_1','MOD_2','MOI','MOS','MOS_1','MOZ','MOZ_1','MOZ_2','MOZ_31','MP','MS','MZ','NAZ','NAZ_1','NAZ_2','NB','NBR','NBR_1','NBR_30','NZ','PAL','PAL_2','PAL_31','PAL_32','PAL_33','PAM','PAM_1','PAM_2','PAM_3','PAM_32','PAM_4','PAT','PAT_1','PAT_2','PAT_3','PAT_31','PGI','PGI_1','PGI_2','PH','PIL','PL','PMI','PMI_2','PP','PRH','PRH_1','PRH_2','PRH_31','PRH_32','PU','PY','RN','RP','RPX','RPX_1','RTR','RTR_1','SC','SCL','SCL_1','SCL_2','SFE','SFE_1','SFE_2','SJO','SJT','SL','SLB','SLB_1','SLB_2','SLB_3','SLB_31','SUC','SUC_1','SUC_2','TAQ','TAQ_1','TAQ_2','TQ','TUC','TUR','TUR_1','TUR_2','TV','UA','URU',];
    let LUZIANIA = ['ABC','ABC_1','ABC_2','ABC_3','ALD','ALD_1','ALD_2','ALD_3','ALD_32','ALD_4','ALD_5','ALD_6','ALD_7','ALD_8','ALF','ALF_1','ASB','ASB_1','CCN','CCN_2','CEC','CEC_1','COC','COC_1','COC_2','COC_3','COC_4','CRI','CRI_1','CRI_32','CRI_33','CRI_35','CRI_36','CRI_38','CRS','CRS_1','CRS_2','CRS_3','CRS_36','CV','DH','DSF','DSF_1','KR','LA','LAJ','LAJ_2','LG','LUZ','LUZ_31','LUZ_32','LZ','LZI','MAJ','MAJ_1','MAJ_2','MAJ_36','MAR','MAR_1','NGA','NGA_1','NGA_2','NJ','PAC','PAC_1','PAC_2','PAC_3','PAC_4','PAC_5','PAC_7','PAC_8','PB','PBE','PBE_1','PBE_2','PBE_31','PBE_32','PMP','PMP_31','PMP_32','PMP_33','PMP_34','PMP_35','POT','PQN','PQN_1','PQN_2','RIQ_2','RIV','RIV_1','RIV_2','RIV_3','RIV_4','RIV_5','RIV_7','RIV_8','RIV_9','RQ','SAD','SAD_1','SAD_2','SBA','SBA_1','SMG','SN','TRB','TRB_1','TRB_2','TRJ','TRJ_1','VA','VAL','VAL_1','VAL_2','VAL_3','ZLN',];
    let METROPOLITANA = ['ACU','ACU_1','ACU_2','AGO','AGO_1','AGO_2','ANH','ANH_1','ANH_2','ANH_3','ANH_4','ANH_5','ANH_6','AO','AP','AR','AV','BLN','BLN_1','BO','BOF','BOF_1','BRZ','BRZ_1','BRZ_2','BV','BVI','BVI_1','BVI_2','BVI_3','BVI_31','BVI_32','BVI_4','BW','CDZ','CDZ_1','CDZ_2','CEP','CEP_1','CEP_2','CEP_3','CEP_30','CEP_32','CEP_33','CEP_5','CK','CPE','CPE_1','CRA','CRA_1','CRA_2','CTI','CTI_1','CTI_2','DE','DL','DML','DML_1','DML_2','DSC','ETR','GA','GC','GL','GML','GML_1','GNI','GNI_1','GNI_2','GNI_3','GR','GUA','GUA_1','GUA_2','GUA_33','HD','HDR','HDR_1','HDR_2','HDR_3','IN','INH','INH_1','INH_2','INH_3','INH_30','INH_31','INH_4','INP','INP_1','INP_10','INP_11','INP_12','INP_2','INP_3','INP_4','INP_5','INP_6','INP_7','INP_8','INP_9','ITC','ITC_1','ITC_2','IX','LB','LBU','LBU_1','LBU_2','LGR','LGR_1','NER','NER_1','NER_3','NER_30','NER_34','NER_4','NOV','NOV_1','NOV_2','NR','NV','OLO','PSL','PSL_1','REL','SB','SBG','SBG_1','SBG_2','SEN','SEN_1','SEN_2','SEN_3','SEN_4','SEN_5','SEN_6','SIL','SIL_1','SIL_2','SIL_30','SIL_33','SMI','SMI_1','SMI_2','SQ','STN','STN_1','STN_2','SV','SW','TQI','TR','TRI','TRI_1','TRI_2','TRI_3','TRI_31','TRI_32','TRI_4','TRI_5','TVR','VAR','VAR_1','VAR_2','VIA','VIA_1','VIA_2','VIA_3','VIA_31','VIA_33','VJ','VN',];
    let MORRINHOS = ['ABU','ABU_1','ABU_2','AD','AGL','AGL_1','AH','AL','ALO','ALO_1','ALO_2','AMD','AMD_1','ARR','ARR_1','BC','BG','BJ','BJE','BJE_1','BJE_2','BNT','BTZ','BTZ_2','BUA','BUA_2','BVT','CAT','CAT_1','CAT_2','CAT_3','CAT_30','CAT_32','CAT_33','CAT_34','CAT_4','CAT_5','CAT_6','CBI','CBI_1','CBI_2','CD','CF','CI','CM','CN','CNO','CNO_1','CNO_2','CNO_3','CNO_4','CNO_5','CPG','CPG_1','CPG_3','CPG_32','CPG_33','CPK','CQ','CRM','CRO','CRO_1','CT','CTP','CTP_1','CTP_2','CUM','CUM_2','CY','DAV','DAV_1','DAV_2','DM','DMI','DMI_1','DMI_3','DMI_5','EA','ED','EDA','EDA_1','EDE','EDE_1','EDE_31','EDE_32','EST','FOG','FOG_1','FOG_2','GD','GOD','GOD_1','GOD_2','GT','GTB','GTB_1','GTB_2','GTB_3','GTB_30','GTB_31','GTB_32','GTB_4','IO','IP','IPA','IPA_1','IT','ITN','ITN_1','ITN_2','ITN_3','ITV','ITV_1','ITV_2','ITV_3','ITV_4','JOV','JOV_1','JOV_2','JOV_31','JOV_32','JS','JV','MAC','MAI','MAI_1','MAZ','MG','MI','MOR','MOR_1','MOR_3','MOR_31','MOR_4','MR','NAU','NAU_1','NE','NUT','ORI','ORI_1','ORI_2','ORI_3','OU','OUV','OUV_1','OUV_2','OZ','PIB','PIB_2','PIJ','PIJ_1','PIJ_2','PIJ_3','PJ','PJA','PJA_1','PJA_2','PK','PLA','PLA_1','PME','PME_1','PME_2','PNM','PNM_1','PNM_2','PON','PON_1','PON_2','PON_31','PR','PRB','PRB_1','PRB_2','PRB_3','PRI','PRI_1','PRI_3','PRI_31','PRI_33','PRI_4','PRI_5','PX','PZ','RIQ','SCA','SCA_1','SCA_2','SCA_3','SCA_4','SRP','SRP_1','TAI','TAI_1','TH','TRR','TRR_2','UCD','UCD_1','UCD_2','UCD_30','UCM','URO','URO_1','URO_3','URO_32','URT','URT_1','UT','VCR','VCR_31','VCR_32','VES','VI','VIC','VIC_1','VIC_2','VIC_32',];
    let RIOVERDE = ['ACN','ACN_1','ACN_2','ACN_3','ACN_31','AE','ALA','AMR','AMR_1','AMR_2','APO','APO_1','APO_30','ARD','ARD_1','ARD_2','AW','BCU','BCU_1','BCU_2','BOS','BZ','CA','CAC','CAC_1','CAC_2','CAE','CAE_1','CAE_31','CAL','CAL_2','CAL_31','CAL_34','CAL_35','CAN','CAS','CAS_30','CBV','CBV_1','CBV_2','CBV_31','CBV_32','CBV_33','CC','CHC','CHC_1','CHC_2','CHC_31','CHC_32','CL','CNH','CNH_1','CNH_2','COA','COB','DJ','DP','ENS','ENS_30','EU','FRT','FRT_1','FRT_2','GB','GOU','GOU_1','GSL','ID','IJ','IM','INA','INA_1','IPG','IPG_1','IPG_2','IPG_3','IPG_4','IPG_5','IPG_6','ITJ','ITJ_1','ITJ_2','ITJ_30','ITM','ITM_1','ITM_2','JAT','JAT_1','JAT_2','JAT_3','JAT_30','JAT_31','JAT_4','JAT_5','JAT_6','JT','LGB','LGB_1','LGB_31','LS','MAU','MAU_1','MAU_2','MIN','MIN_1','MIN_2','MIN_3','MIN_31','MIN_4','ML','MNS','MNS_4','MOT','MOT_1','MOT_2','MOT_3','MOT_31','MOT_32','MOT_33','MV','MY','NAV','NAV_2','PAG','PAG_1','PAG_2','PAG_3','PAG_31','PAG_32','PC','PER','PER_1','PQE','PQE_30','PQE_32','PQE_34','PQE_35','PRD','PRT','PRT_1','PRT_2','PSS','PTE','PTE_1','PTE_2','PW','QI','QUI','QUI_1','QUI_2','QUI_3','QUI_31','QUI_32','RBO','RBO_31','RBO_32','RBO_33','RCL','RCL_33','RIP','RIP_1','RIP_2','RV','RVE','RVE_1','RVE_2','RVE_3','RVE_31','RVE_4','RVE_5','RVE_6','RVE_7','RVE_8','RVF','RVL','RVL_1','RVL_2','SAT','SAT_1','SAT_2','SAZ','SAZ_31','SAZ_32','SAZ_33','SER','SER_1','SH','SHE','SHE_1','SHE_2','SHE_3','SHE_31','SHE_33','SHE_34','SHE_5','SO','SRA','SRA_2','SRI','SRI_1','SSI','SSI_1','SSI_2','SU','SX','TD','TQR','TUV','TUV_1','TUV_2',];
    let URUACU = ['AHZ','ANV','ANV_1','ANV_2','BON','BON_1','BON_2','BRR','BRR_1','BRR_2','BX','CDN','CLC','CLC_1','CLC_2','CNT','CPN','CPN_1','CPN_2','CR','CVE','CVE_1','CX','CXA','CXA_1','CXA_2','ENO','ENO_1','FO','FOM','FOM_1','GF','HID','HID_31','HL','IH','ITD','ITD_1','ITD_2','ITD_32','ITP','ITP_1','ITP_2','ITP_32','IW','LC','MH','MIC','MIC_1','MIC_2','MIC_3','MIC_31','MQ','MRO','MRO_1','MRO_2','MRO_3','MRO_31','MU','MUN','MUT','MUT_1','MUT_2','MX','NA','NAM','NAM_1','NAM_33','NCS','NCS_1','NCS_31','NCS_32','NG','NH','NIG','NIG_1','NIG_2','NIG_3','NIQ','NIQ_1','NIQ_2','NIQ_3','NIQ_32','NM','NOP','NOP_1','NPL','NPL_1','NPL_2','NPL_31','NQ','NQL','NQL_31','NQL_32','PGO','PO','POR','POR_1','POR_2','POR_3','POR_31','POR_33','QBL','QBL_2','RB','RUB','RUB_1','RUB_2','RUB_3','RUB_31','RUB_32','SE','SG','SM','SMA','SMA_1','SMA_2','SMA_3','SMA_31','SMA_32','SOU','STA','STA_1','STE','STE_31','STE_32','STE_33','STE_35','STE_5','SZ','TA','TB','UC','UP','UR','URC','URC_1','URC_2','URC_3','URC_31','URC_32','VD','XIX','XIX_2',];
    let OperName = ['RHYAN','AFONSO','CARLA','DOUGLAS','GOULART','LUCAS','VICTOR','ACÁCIO','ALBERT','ALEX','ALIOMAR','ARILSON','BIÉRCIO','BRUNA','RAGONETE','BRUNO','CARLOS','DANIEL','DIEGO','DOVAL','EMERSON','ETERNO','INÁCIO','FERNANDO','CHAGAS','FRANCISCO','GABRIEL','GELSON','NETO','HIAGO','HUMBERTO','JAILSON','JOHNATAN','JONAS','FRANKLIN','JOSÉ MÁRCIO','JUNIELSON','LUAN','PEREIRA','MORAIS','LUCIANO','LUIZ','RIBEIRO','MARCELO','MARCIUS','MAURÍCIO','MEIRE','MICHEL','MIGUEL','NEUBER','NEUTON','PATRÍCIO','PAULO','OLIVEIRA','REINALDO','SILVA','RICARDO','RONIERE','RUI','SANDRO','SÉRGIO','THALLYTON','LEÃO','FARIAS','GONZAGA','VALDIR','WALMIR','WELDER','LOURENÇO','DANILO','EDMAR','ELIEL','HENRIQUE','RIBAS','WELTON'];
    let OperId =['120406','124552','124760','124618','124278','123237','121502','112756','123353','120091','116099','124680','116853','124620','124047','124199','111960','120054','110310','101450','124205','124217','123845','123675','116087','092393','124606','118280','124242','124084','120303','124631','124266','111673','114212','102465','123742','123833','124308','124679','112835','124126','117778','112495','117869','121083','124310','110425','123869','124072','124590','123870','123651','123894','124588','124321','123687','113141','124709','111387','112185','124345','110383','123857','123882','123663','99727','124370','124138','112653','112689','99570','120662','108110','117328'];
    let getRegion = (initialName)=>{
        let region = "";
        if(ANAPOLIS.includes(initialName)){
            region = "ANAPOLIS";
        }else
        if(FORMOSA.includes(initialName)){
            region = "FORMOSA";
        }else
        if(GOIANIA.includes(initialName)){
            region = "GOIANIA";
        }else
        if(IPORA.includes(initialName)){
            region = "IPORA";
        }else
        if(LUZIANIA.includes(initialName)){
            region = "LUZIANIA";
        }else
        if(METROPOLITANA.includes(initialName)){
            region = "METROPOLITANA";
        }else
        if(MORRINHOS.includes(initialName)){
            region = "MORRINHOS";
        }else
        if(RIOVERDE.includes(initialName)){
            region = "RIO VERDE";
        }else
        if(URUACU.includes(initialName)){
            region = "URUACU";
        }
        return region;
    };
    let searchEachTicket = function(ajaxUrl,equipToSearch,ticketNumberId,sourcer){
        GM_xmlhttpRequest({
            method: "GET",
            url: ajaxUrl,
            onload : function(res){
                if(res.status == 200){
                    let doc = new DOMParser();
                    doc = doc.parseFromString(res.responseText, "text/html");
                    let equipamentTable = "EQUIPAMENTO";
                    if(doc.querySelectorAll("#preview > table:nth-child(5) > tbody > tr > th").length > 0){
                        if(doc.querySelector("#preview > table:nth-child(5) > tbody > tr > th").innerText.toUpperCase().includes("EQUIPAMENTO")){
                            equipamentTable = doc.querySelector("#preview > table:nth-child(5) > tbody > tr > td").innerText.toUpperCase();
                        }
                    }
                    if(doc.querySelector("#preview > table:nth-child(1) > tbody > tr:nth-child(1) > td").innerText.toUpperCase().includes("CLOSE")){
                        if(equipamentTable == equipToSearch){
                            //console.log("fechado");
                            //console.log(ticketNumberId);
                        }
                    }else{
                        if(equipamentTable == equipToSearch){
                            //console.log("aberto");
                            //console.log(ticketNumberId);
                            sourcer.append(document.createElement("br"));
                            sourcer.append("TICKET: "+ticketNumberId);
                        }
                    }
                }
            }
        });
    };
    let searchTicket = function(equipToSearch,sourcer,ticketSE){
        let querySearch = equipToSearch;
        if(ticketSE){
            querySearch = equipToSearch.slice(0, 3);
        }
        GM_xmlhttpRequest({
            method: "GET",
            headers:{
                "X-Requested-With": "XMLHttpRequest",
                "X-PJAX": "true",
                "X-PJAX-Container": "#pjax-container"
            },
            url: 'http://10.154.77.34/osticket/scp/tickets.php?a=search&search-type=&query='+querySearch+'&_pjax=%23pjax-container&sort=2&dir=1',
            onload : function(res){
                if(res.finalUrl.includes("login.php")){
                   let myonpen = GM_openInTab("http://10.154.77.34/osticket/scp/login.php", {loadInBackground :true});
                   setTimeout(function(){
                       myonpen.close();
                   },10000);
                }else{
                    if(res.status == 422 || res.status == 200){
                        let doc = new DOMParser();
                        doc = doc.parseFromString(res.responseText, "text/html");
                        sourcer.append(document.createElement("br"));
                        if(doc.querySelector("#tickets > table > tbody").children.length > 0){
                            let ticketLine = doc.querySelectorAll("#tickets > table > tbody > tr");
                            let ticketLineCrl = ticketLine.length;
                            ticketLine.forEach((elem,index)=>{
                                let ticketNumberId = elem.querySelector("td:nth-child(2) a[class=preview]").innerText;
                                searchEachTicket("http://10.154.77.34/osticket/scp/ajax.php/"+elem.querySelector("a[class=preview]").getAttribute("data-preview").replace("#",""),equipToSearch,ticketNumberId,sourcer);
                            });
                        }
                    }
                }
            }
        });
    };

    $(document).ajaxComplete(function() {
        document.querySelectorAll("#equipamentos > .card-deck > div").forEach((elem,index)=>{
            if(elem.classList.contains("piscar")){
                setInterval(function(){
                    if(elem.querySelector("div.card-body").style.background == "rgb(76, 175, 80)"){
                        elem.querySelector("div.card-body").style.background = "";
                    }else{
                        elem.querySelector("div.card-body").style.background = "rgb(76, 175, 80)";
                    }
                },700);
            }
            let curOperId = elem.querySelector("p.card-text.text-center").innerText.split("(")[1].split(")")[0];
            let equipament = elem.querySelector(".apagar > b").innerText;
            let ticketSE = false;
            let sourcer = elem.querySelector("h5");
            let region = "";
            if(equipament.length == 10){
                region = getRegion(equipament.slice(0, 2));
            }else{
                region = getRegion(equipament.slice(0, 3));
                ticketSE = true;
            }
            sourcer.innerText = sourcer.innerText+" "+region;
            sourcer.append(document.createElement("br"));
            if(OperId.includes(curOperId)){
                sourcer.append(OperName[OperId.indexOf(curOperId)]);
            }else{
                sourcer.append("OP. NÃO IDENTIFICADO");
            }
            if(!denyStorage){
                let totalEquip = Object.values(JSON.parse(localStorage.getItem("totalEquip")));
                let availability = Object.values(JSON.parse(localStorage.getItem("availability")));
                if(totalEquip.includes(equipament)){
                    sourcer.append(document.createElement("br"));
                    sourcer.append(document.createElement("br"));
                    sourcer.append("DISP.30 DIAS: "+availability[totalEquip.indexOf(equipament)]+"%");
                    totalEquip = null;
                    availability = null;
                }
            }
            //searchEquipment(equipament,sourcer);
            searchTicket(equipament,sourcer,ticketSE);
        });
    });
});