// ==UserScript==
// @name         OSticket Treatment Helper
// @version      2.23
// @downloadURL  https://bitbucket.org/teuzer/noc-automation/raw/master/OSticketHelper.user.js
// @updateURL    https://bitbucket.org/teuzer/noc-automation/raw/master/OSticketHelper.user.js
// @description  Botoes e barra auxiliar para tratamento OSticket
// @author       Teuzer
// @match        http://10.154.77.34/osticket/scp/tickets.php*
// @match        http://e6aaziosta00/osticket/scp/tickets.php*
// @grant        none
// ==/UserScript==

(function(){
    class ajudadorPessoal extends HTMLElement {
        constructor() {
            super();
            const shadow = this.attachShadow({mode: 'open'});

            const wrapper = document.createElement('div');
            wrapper.setAttribute('class', 'w3-col s12 w3-right-align');
            wrapper.style.background = "#f4f4f4";
            wrapper.style.display = "table";
            wrapper.style.height = "40px";
            wrapper.style.borderRadius = "0 0 5px 5px";
            wrapper.style.boxShadow = "0 1px 1px 1px #d2d2d2";

            const W3CSS = document.createElement('link');
            W3CSS.setAttribute('rel', 'stylesheet');
            W3CSS.setAttribute('href', 'https://www.w3schools.com/w3css/4/w3.css');

            let BTN_STATUS = document.createElement('button');
            let backgroundColor = ()=>{
                setTimeout(()=>{
                    if(document.querySelector("#tickets > table > tbody").children.length > 0){
                        BTN_STATUS.removeEventListener("click",backgroundColor,false);
                        BTN_STATUS.classList.remove("w3-blue");
                        BTN_STATUS.classList.add("w3-grey");
                        let ticketLine = document.querySelectorAll("#tickets > table > tbody > tr");
                        let ticketLineCrl = ticketLine.length;
                        ticketLine.forEach((elem,index)=>{
                            if(elem.querySelectorAll("td:nth-child(2) span.Icon").length > 0){
                                elem.querySelector("td:nth-child(2) span.Icon").style.display = "none";
                            }
                            if(elem.querySelectorAll("td:nth-child(4) span.pull-left").length > 0){
                                elem.querySelector("td:nth-child(4) span.pull-left").style.display = "none";
                            }
                            if(elem.querySelectorAll("td:nth-child(4) span.pull-right").length > 0){
                                elem.querySelector("td:nth-child(4) span.pull-right").style.display = "none";
                            }
                            elem.querySelectorAll("a").forEach((a)=>{a.style.color = "#000000";a.style.borderBottom = "1px solid black";a.style.display = "table";a.style.fontWeight = "normal";});
                            let ticketNumberId = elem.querySelector("td:nth-child(2) a[class=preview]");
                            //ticketNumberId.style.background = "#FFFFFF";
                            ticketNumberId.style.borderRadius = "2px";
                            ticketNumberId.style.padding = "3px";
                            ticketNumberId.style.margin = "auto";
                            ticketNumberId.style.fontSize = "1.3rem";
                            let innerReadbility = ticketNumberId.innerText.match(/.{1,2}/g);
                            if(!ticketNumberId.innerText.includes(" ")){
                                ticketNumberId.innerText = innerReadbility[0]+" "+innerReadbility[1]+" "+innerReadbility[2];
                            }
                            ticketNumberId.parentNode.style.verticalAlign = "middle";
                            let jqxhr = $.get( "http://10.154.77.34/osticket/scp/ajax.php/"+elem.querySelector("a[class=preview]").getAttribute("data-preview").replace("#",""))
                            .done(function(data) {
                                let doc = new DOMParser();
                                doc = doc.parseFromString(data, "text/html");
                                let equipamentTable = "EQUIPAMENTO";
                                let causeTable = "";
                                if(doc.querySelectorAll("#preview > table:nth-child(5) > tbody > tr > th").length > 0){
                                    if(doc.querySelector("#preview > table:nth-child(5) > tbody > tr > th").innerText.toUpperCase().includes("EQUIPAMENTO")){
                                        equipamentTable = doc.querySelector("#preview > table:nth-child(5) > tbody > tr > td").innerText.toUpperCase();
                                    }
                                }
                                if(doc.querySelectorAll("#preview > table:nth-child(11) > tbody > tr > th").length > 0){
                                    if((doc.querySelector("#preview > table:nth-child(11) > tbody > tr > th").innerText.toUpperCase().includes("STATUS"))||
                                       (doc.querySelector("#preview > table:nth-child(11) > tbody > tr > th").innerText.toUpperCase().includes("CAUSA"))){
                                        causeTable = doc.querySelector("#preview > table:nth-child(11) > tbody > tr > td").innerText.toUpperCase();
                                    }
                                }
                                elem.querySelector("td:nth-child(6)").innerHTML = equipamentTable;
                                //elem.querySelector("td:nth-child(4)").innerHTML += "<hr style='border-width: 1px 0 0;border-color: black;border-style: solid;margin: 2px 0;'>"+causeTable;
                                if(doc.querySelector("#preview > table:nth-child(1) > tbody > tr:nth-child(1) > td").innerText.includes("Close")){
                                    $(elem).children().css({"background":"#fd9898","border-width":"0px 0px 2px","border-color":"#000","box-shadow":"0px 0px 0px 1px #626262"});
                                }else{
                                    if(doc.querySelectorAll("#preview > table:nth-child(3) > tbody > tr:nth-child(1) > td").length > 0){
                                        let responsableTable = doc.querySelector("#preview > table:nth-child(3) > tbody > tr:nth-child(1) > td").innerText.split("/");
                                        if(responsableTable.length > 1 ){
                                           elem.querySelector("td:nth-child(7)").innerHTML = responsableTable[1];
                                        }
                                    }
                                    //console.log(doc.querySelector("#preview > table:nth-child(3) > tbody > tr:nth-child(1) > td").innerText);
                                    //console.log(elem.querySelector("a[class=preview]").innerText);
                                    $(elem).children().css({"background":"#92d49a","border-width":"0px 0px 2px","border-color":"#000","box-shadow":"0px 0px 0px 1px #626262"});
                                }
                            })
                            .fail(function() {
                                console.log( "error" );
                            })
                            .always(function() {
                                ticketLineCrl = ticketLineCrl - 1; console.log(ticketLineCrl);
                                if(ticketLineCrl == 0){
                                    BTN_STATUS.classList.remove("w3-grey");
                                    BTN_STATUS.classList.add("w3-blue");
                                    BTN_STATUS.addEventListener("click",backgroundColor,false);
                                }
                                //console.log( "finished" );
                            });
                        });
                    }
                },100);
            };
            let aListener = function(){
                document.querySelectorAll("a").forEach((aElem,Aindex)=>{
                    aElem.addEventListener("click",backgroundColor,false);
                });
            };

            BTN_STATUS.innerText = "Busca Status";
            BTN_STATUS.style.position = "relative";
            BTN_STATUS.style.top = "4px";
            BTN_STATUS.setAttribute('class', ' w3-btn w3-blue w3-hover-indigo w3-round large');
            BTN_STATUS.addEventListener("click",backgroundColor,false);

            let BTN_OPEN = document.createElement('button');
            BTN_OPEN.innerText = "Abrir ticket";
            BTN_OPEN.style.position = "relative";
            BTN_OPEN.style.top = "4px";
            BTN_OPEN.setAttribute('class', ' w3-btn w3-green w3-margin-left w3-hover-teal w3-round large');
            BTN_OPEN.addEventListener("click",()=>{
                window.open("http://10.154.77.34/osticket/open.php","_blank");
            },false);

            let BTN_SELECTALL = document.createElement('button');
            BTN_SELECTALL.innerText = "Selecionar Todos";
            BTN_SELECTALL.style.position = "relative";
            BTN_SELECTALL.style.top = "4px";
            BTN_SELECTALL.setAttribute('class', ' w3-btn w3-pale-green w3-margin-right w3-hover-teal w3-round large');
            BTN_SELECTALL.addEventListener("click",()=>{
                let allRow = document.querySelectorAll("table.list.queue.tickets tbody tr td:nth-child(1)");
                if(allRow.length > 0){
                    allRow.forEach((elem)=>{
                        elem.parentNode.querySelector("td input").click();
                    });
                }
            },false);

            shadow.appendChild(W3CSS);
            shadow.appendChild(wrapper);
            wrapper.appendChild(BTN_SELECTALL);
            wrapper.appendChild(BTN_STATUS);
            wrapper.appendChild(BTN_OPEN);
            let inputSearch = document.createElement("input");
            inputSearch.placeholder = "Equipamento";
            inputSearch.style.position = "absolute";
            inputSearch.style.top = "3px";
            inputSearch.style.left = "8px";
            inputSearch.style.width = "270px";
            inputSearch.style.height = "35px";
            inputSearch.style.fontSize = "16px";
            inputSearch.style.padding = "5px 7px";
            inputSearch.style.border = "2px solid black";
            inputSearch.style.borderRadius = "5px 0 0 5px";
            inputSearch.value = (document.querySelectorAll("#basic_search input.basic-search").length > 0)?document.querySelector("#basic_search input.basic-search").value:"";
            inputSearch.setAttribute('id', '#mine-searcher');

            let buttonSearch = document.createElement("button");
            buttonSearch.innerText = "Pesquisar";
            buttonSearch.style.position = "absolute";
            buttonSearch.style.top = "3px";
            buttonSearch.style.left = "270px";
            buttonSearch.style.width = "100px";
            buttonSearch.style.height = "35px";
            buttonSearch.style.fontSize = "16px";
            buttonSearch.style.border = "2px solid black";
            buttonSearch.style.borderRadius = "0 5px 5px 0";
            buttonSearch.style.background = "#2196f3";
            buttonSearch.style.cursor = "pointer";
            buttonSearch.setAttribute('id', '#mine-button-searcher');
            buttonSearch.addEventListener("click",(event)=>{
                event.preventDefault();
                event.stopPropagation();
                window.location.href = "http://10.154.77.34/osticket/scp/tickets.php?a=search&search-type=&query="+inputSearch.value+"&sort=2&dir=1";
            });

            inputSearch.addEventListener("keydown",(e)=>{
                if(e.key == "Enter" || e.keyCode == 13){
                    buttonSearch.click();
                }
            },false);

            let upScroll = document.createElement("div");
            upScroll.innerText = "🢁";
            upScroll.style.position = "fixed";
            upScroll.style.bottom = "49vh";
            upScroll.style.right = "8px";
            upScroll.style.width = "50px";
            upScroll.style.height = "50px";
            upScroll.style.fontSize = "26px";
            upScroll.style.color = "white";
            upScroll.style.borderRadius = "50%";
            upScroll.style.lineHeight = "42px";
            upScroll.style.textAlign = "center";
            upScroll.style.background = "#4caf50";
            upScroll.style.cursor = "pointer";
            upScroll.setAttribute('id', '#mine-scroll');
            upScroll.addEventListener('click',()=>{ window.scrollTo(0, 0); },false);

            wrapper.prepend(buttonSearch);
            wrapper.prepend(inputSearch);
            wrapper.prepend(upScroll);
        }
    }

    customElements.define('ajudador-pessoal', ajudadorPessoal);

    let shadow = document.createElement('ajudador-pessoal');
    shadow.style.position = "absolute";
    shadow.style.top = "108px";
    shadow.style.zIndex = "98";
    shadow.style.width = "100%";
    shadow.style.height = "42px";
    shadow.style.overflow = "hidden";
    shadow.style.boxSizing = "border-box";
    document.querySelector("#header").style.position = "relative";
    document.querySelector("#header").appendChild(shadow);

     class treatmenthelper extends HTMLElement {
        constructor() {
            super();
            const shadow = this.attachShadow({mode: 'closed'});

            let inputResult = document.createElement("textarea");
            inputResult.setAttribute('id', 'inputResult');;
            inputResult.style.position = "absolute";
            inputResult.style.left = "0px";
            inputResult.style.top = "0px";
            inputResult.style.opacity = "0";
            inputResult.style.height = "0px";
            inputResult.style.width = "0px";
            document.querySelector("body").appendChild(inputResult);

            const BTNRESULTEQUIP = document.createElement("button");
            BTNRESULTEQUIP.innerText = "Copiar Equipamento";
            BTNRESULTEQUIP.style.position = "fixed";
            BTNRESULTEQUIP.style.right = "0px";
            BTNRESULTEQUIP.style.top = "25vh";
            BTNRESULTEQUIP.style.display = "block";
            BTNRESULTEQUIP.style.padding = "14px";
            BTNRESULTEQUIP.style.background = "#4caf50";
            BTNRESULTEQUIP.style.border = "none";
            BTNRESULTEQUIP.style.borderRadius = "10px";
            BTNRESULTEQUIP.style.fontSize = "16px";
            BTNRESULTEQUIP.style.fontWeight = "bold";
            let CopyResultEquipament = ()=>{
                document.querySelectorAll("a.ticket-action").forEach((elem,index)=>{
                    if(elem.parentElement.previousElementSibling !== null){
                        if(elem.parentElement.previousElementSibling.innerText.toUpperCase().startsWith("EQUIPAMENTO")){
                            inputResult.value = elem.innerText.toUpperCase();
                        }
                    }
                });
                inputResult.select();
                document.execCommand("copy");
                BTNRESULTEQUIP.innerText = "Copiado!";
                setTimeout(()=>{
                    BTNRESULTEQUIP.innerText = "Copiar Equipamento";
                },1000);
            };
            BTNRESULTEQUIP.addEventListener("click",CopyResultEquipament,false);

            const BTNRESULTTICKET = document.createElement("button");
            BTNRESULTTICKET.innerText = "Copiar Ticket";
            BTNRESULTTICKET.style.position = "fixed";
            BTNRESULTTICKET.style.right = "0px";
            BTNRESULTTICKET.style.top = "15vh";
            BTNRESULTTICKET.style.display = "block";
            BTNRESULTTICKET.style.padding = "14px";
            BTNRESULTTICKET.style.background = "#2196f3";
            BTNRESULTTICKET.style.border = "none";
            BTNRESULTTICKET.style.borderRadius = "10px";
            BTNRESULTTICKET.style.fontSize = "16px";
            BTNRESULTTICKET.style.fontWeight = "bold";
            let CopyResultTicket = ()=>{
                inputResult.value = document.querySelector("h2").innerText.split("#")[1].replace(/ /g,"");
                inputResult.select();
                document.execCommand("copy");
                BTNRESULTTICKET.innerText = "Copiado!";
                setTimeout(()=>{
                    BTNRESULTTICKET.innerText = "Copiar Ticket";
                },1000);
            };
            BTNRESULTTICKET.addEventListener("click",CopyResultTicket,false);

            let selectorFunction = ()=>{
                if(document.querySelectorAll("#inline_update div > select > option").length == 0){
                    setTimeout(()=>{selectorFunction();},1000);
                }else{
                    document.querySelectorAll("#inline_update div > select > option").forEach((elem)=>{
                        if(elem.innerText.includes("NÃO IDENTIFICADO")){
                            elem.parentNode.value = elem.value;
                        }
                    });
                    setTimeout(()=>{
                        document.querySelector("#inline_update .buttons.pull-right>input").click();
                    },500);
                }
            };

            let selectorFunctionAssign = (destine)=>{
                if(document.querySelectorAll("form#assign select option").length == 0){
                    setTimeout(()=>{selectorFunctionAssign(destine);},1000);
                }else{
                    document.querySelectorAll("form#assign select option").forEach((elem)=>{
                        if(elem.innerText.toUpperCase().includes(destine)){
                            elem.parentNode.value = elem.value;
                        }
                    });
                    setTimeout(()=>{
                        document.querySelector("form.mass-action .buttons.pull-right>input").click();
                    },500);
                }
            };

            let selectorFunctionClose = ()=>{
                if(document.querySelectorAll("form#status .buttons.pull-right>input").length == 0){
                    setTimeout(()=>{selectorFunctionClose();},1000);
                }else{
                    setTimeout(()=>{
                        document.querySelector("form#status .buttons.pull-right>input").click();
                    },500);
                }
            };

            const wrapper = document.createElement('div');
            wrapper.setAttribute('class', 'w3-col s12 w3-center w3-padding');
            wrapper.style.background = "#f4f4f4";
            wrapper.style.height = "100%";

            const W3CSS = document.createElement('link');
            W3CSS.setAttribute('rel', 'stylesheet');
            W3CSS.setAttribute('href', 'https://www.w3schools.com/w3css/4/w3.css');

            let BTN_CAUSE = document.createElement('button');
            BTN_CAUSE.innerHTML = "ATUALIZAR CAUSA</br>NÃO IDENTIFICADO";
            BTN_CAUSE.setAttribute('class', ' w3-btn w3-teal w3-hover-dark-grey w3-round large w3-section');
            BTN_CAUSE.addEventListener("click",()=>{
                document.querySelectorAll("tr > td > a").forEach((elem)=>{
                    if(elem.parentNode.previousElementSibling != null){
                        if(elem.parentNode.previousElementSibling.innerText.includes("STATUS")||elem.parentNode.previousElementSibling.innerText.includes("CAUSA")){
                            if(elem.querySelectorAll("span.faded").length > 0){
                                elem.click();
                                selectorFunction();
                            }
                        }
                    }
                });
            },false);

            let BTN_ASSIGN = document.createElement("button");
            BTN_ASSIGN.innerText = "ATRIBUIR DESPACHO";
            BTN_ASSIGN.setAttribute('class', ' w3-btn w3-indigo w3-hover-dark-grey w3-round large w3-section');
            BTN_ASSIGN.addEventListener("click",(event)=>{
                document.querySelectorAll("a[href*='assign']").forEach((elem)=>{
                    if(elem.parentNode.previousElementSibling != null){
                        if(elem.parentNode.previousElementSibling.innerText.includes("Atribuído a")){
                            if(!elem.innerText.toUpperCase().includes("DESPACHO")){
                                document.querySelector("#content div.sticky.bar li a[href*='assign/teams']").click();
                                selectorFunctionAssign("DESPACHO");
                            }
                        }
                    }
                });
            });

            let BTN_ASSIGN_ENEL = document.createElement("button");
            BTN_ASSIGN_ENEL.innerText = "ATRIBUIR EQUIPES ENEL";
            BTN_ASSIGN_ENEL.setAttribute('class', ' w3-btn w3-teal w3-hover-dark-grey w3-round large w3-section');
            BTN_ASSIGN_ENEL.addEventListener("click",(event)=>{
                document.querySelectorAll("a[href*='assign']").forEach((elem)=>{
                    if(elem.parentNode.previousElementSibling != null){
                        if(elem.parentNode.previousElementSibling.innerText.includes("Atribuído a")){
                            if(!elem.innerText.toUpperCase().includes("EQUIPES ENEL")){
                                document.querySelector("#content div.sticky.bar li a[href*='assign/teams']").click();
                                selectorFunctionAssign("EQUIPES ENEL");
                            }
                        }
                    }
                });
            });

            let BTN_CLOSE = document.createElement("button");
            BTN_CLOSE.innerText = "FECHAR TICKET";
            BTN_CLOSE.setAttribute('class', ' w3-btn w3-red w3-hover-dark-grey w3-round large w3-section');
            BTN_CLOSE.addEventListener("click",(event)=>{
                document.querySelectorAll("tr > td > a").forEach((elem)=>{
                    if(elem.parentNode.previousElementSibling != null){
                        if(elem.parentNode.previousElementSibling.innerText.includes("Status")){
                            if(elem.innerText.includes("Open")){
                                document.querySelector("#content div.sticky.bar li a[href*='status/'] i.icon-ok-circle").parentNode.click();
                                selectorFunctionClose();
                            }
                        }
                    }
                });
            });
            wrapper.appendChild(BTN_CAUSE);
            wrapper.appendChild(BTN_ASSIGN);
            wrapper.appendChild(BTN_ASSIGN_ENEL);
            wrapper.appendChild(BTN_CLOSE);
            wrapper.appendChild(BTNRESULTTICKET);
            wrapper.appendChild(BTNRESULTEQUIP);
            shadow.appendChild(W3CSS);
            shadow.appendChild(wrapper);
        }
    }
    customElements.define('treatment-helper', treatmenthelper);

    let shadowTreatment = document.createElement('treatment-helper');
    shadowTreatment.style.position = "fixed";
    shadowTreatment.style.bottom = "0";
    shadowTreatment.style.right = "0";
    shadowTreatment.style.zIndex = "98";
    shadowTreatment.style.width = "200px";
    shadowTreatment.style.minHeight = "38vh";
    shadowTreatment.style.overflow = "hidden";
    shadowTreatment.style.boxSizing = "border-box";
    shadowTreatment.style.display = "none";
    document.querySelector("body").appendChild(shadowTreatment);
    const underStatusLine = document.createElement('div');
    underStatusLine.setAttribute('id', 'underStatusLine');
    underStatusLine.style.position = "fixed";
    underStatusLine.style.bottom = 0;
    underStatusLine.style.left = 0;
    underStatusLine.style.zIndex = 98;
    underStatusLine.style.height = "10px";
    underStatusLine.style.width = "100%";
    document.querySelector("body").appendChild(underStatusLine);
    let readTicketStatus = ()=>{
        setTimeout(()=>{
            let ticketNumberId = document.querySelector("h2").innerText.trim().split("#");
            let innerReadbility = ticketNumberId[1].match(/.{1,2}/g);
            if(!ticketNumberId[1].includes(" ")){
                ticketNumberId[1] = innerReadbility[0]+" "+innerReadbility[1]+" "+innerReadbility[2];
            }
            let CopyResultTicketTitle = ()=>{
                let inputResult = document.querySelector("#inputResult");
                inputResult.value = document.querySelector("h2").innerText.split("#")[1].replace(/ /g,"");
                inputResult.select();
                document.execCommand("copy");
                document.querySelector("h2").style.color = "#CCCCCC";
                setTimeout(()=>{
                    document.querySelector("h2").style.color = "#0A568E";
                },750);
            };
            document.querySelector("h2").removeEventListener("click",CopyResultTicketTitle,false);
            document.querySelector("h2").style.cursor = "pointer";
            document.querySelector("h2").addEventListener("click",CopyResultTicketTitle,false);

            let tableColor = "#FFFFFF";
            let trColor = "#FFFFFF";
            document.querySelector("h2").innerText = ticketNumberId.join(" #");
            document.querySelectorAll("tr > td > a").forEach((elem)=>{
                if(elem.parentNode.previousElementSibling != null){
                    if(elem.parentNode.previousElementSibling.innerText.includes("Status")){
                        if(elem.innerText.includes("Open")){
                            console.log("abert");
                            document.querySelector("#underStatusLine").style.background = "#92d49a";
                            tableColor = "#92d49a";
                            trColor = "#def9e1";
                        }else{
                            console.log("fechad");
                            document.querySelector("#underStatusLine").style.background = "#fd7272";
                            tableColor = "#fd7272";
                            trColor = "#ffadad";
                        }
                    }
                }
            });
            document.querySelectorAll("table.ticket_info").forEach((elem,index)=>{
                elem.style.background = tableColor;
                elem.setAttribute("cellspacing",0);
                elem.setAttribute("cellpadding",0);
                elem.querySelector("tbody").style.verticalAlign = "top";
                elem.querySelectorAll("td:last-child").forEach((innerTd)=>{
                    if(innerTd.innerText.trim().length == 0){
                        innerTd.style.display = "none";
                    }
                });
                elem.querySelectorAll("table").forEach((innerElem)=>{
                    innerElem.setAttribute("cellspacing",2);
                    innerElem.setAttribute("cellpadding",4);
                    innerElem.querySelectorAll("tr:nth-child(odd)").forEach((trOdd)=>{
                        trOdd.style.background = trColor;
                    });
                    innerElem.querySelectorAll("tr:nth-child(even)").forEach((trOdd)=>{
                        trOdd.style.background = "#FFFFFF";
                    });
                });
            });
            document.querySelectorAll("table.custom-data").forEach((elem,index)=>{
                elem.style.background = tableColor;
                elem.setAttribute("cellspacing",2);
                elem.setAttribute("cellpadding",0);
                elem.querySelector("tbody").style.verticalAlign = "top";
                elem.querySelectorAll("tr:nth-child(even)").forEach((trOdd)=>{
                    trOdd.style.background = trColor;
                });
                elem.querySelectorAll("tr:nth-child(odd)").forEach((trOdd)=>{
                    trOdd.style.background = "#FFFFFF";
                });
                elem.querySelectorAll("th").forEach((th)=>{
                    th.style.background = trColor;
                    th.style.border = "none";
                });
            });
            document.querySelector("#container").style.boxShadow = "0 -5px 0 5px "+tableColor;
        },700);
    }
    let _wr = function(type) {
        let orig = history[type];
        return function() {
            let rv = orig.apply(this, arguments);
            let e = new Event(type);
            e.arguments = arguments;
            window.dispatchEvent(e);
            return rv;
        };
    };
    history.pushState = _wr('pushState');
    history.replaceState = _wr('replaceState');
    let checkUrl = ()=>{
        if(window.location.href.includes("tickets.php?id=")){
            shadowTreatment.style.display = "block";
            readTicketStatus();
        }else{
            shadowTreatment.style.display = "none";
            document.querySelector("#underStatusLine").style.background = "";
        }
    }
    checkUrl();
    window.addEventListener('replaceState', function(e) {
        checkUrl();
    });

})();