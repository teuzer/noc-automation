// ==UserScript==
// @name         logOSArtoPanel
// @version      2.1
// @downloadURL  https://bitbucket.org/teuzer/noc-automation/raw/master/logOSArtoPanel.user.js
// @updateURL    https://bitbucket.org/teuzer/noc-automation/raw/master/logOSArtoPanel.user.js
// @description  Loga no OSticket para uso de requisições do portal ARTO
// @author       Teuzer | BR0034864741
// @match        http://10.154.77.34/osticket/scp/*
// @grant        none
// ==/UserScript==

(function() {
    if(["http://10.154.77.34/osticket/scp/login.php"].includes(window.location.href)){
        document.querySelector("fieldset #name").value = "BR0034864741";
        document.querySelector("fieldset #pass").value = "Abril123321'";
        document.querySelector("fieldset button").click();
    }
})();