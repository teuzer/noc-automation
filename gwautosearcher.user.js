// ==UserScript==
// @name         Auto Searcher
// @version      2.1
// @downloadURL  https://bitbucket.org/teuzer/noc-automation/raw/master/gwautosearcher.user.js
// @updateURL    https://bitbucket.org/teuzer/noc-automation/raw/master/gwautosearcher.user.js
// @description  Botao de auto pesquisar a cada 3 segundos
// @author       Teuzer
// @match        http://10.125.18.66:8080/Axon4/event/controlPanelActi*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    // Create a class for the element
    class ajudadorPessoal extends HTMLElement {
        constructor() {
            super();

            const shadow = this.attachShadow({mode: 'closed'});

            const wrapper = document.createElement('div');
            wrapper.setAttribute('class', 'w3-col s12 w3-padding w3-light-grey w3-topbar');

            const W3CSS = document.createElement('link');
            W3CSS.setAttribute('rel', 'stylesheet');
            W3CSS.setAttribute('href', 'https://www.w3schools.com/w3css/4/w3.css');

            let icon = document.createElement('i');
            icon.setAttribute('class','fas fa-play');

            let buttonAutoSearch = document.createElement('button');
            buttonAutoSearch.innerText = "Auto Pesquisar ►";
            buttonAutoSearch.appendChild(icon);
            buttonAutoSearch.setAttribute('class', 'w3-btn w3-green w3-round-large w3-hover-teal w3-border w3-border-white');
            let searchStatus = false;
            let clicador = null;
            buttonAutoSearch.addEventListener("click",function(){
                this.classList.remove('w3-green');
                this.classList.remove('w3-hover-teal');
                this.classList.add('w3-pale-red');
                this.classList.add('w3-hover-red');
                this.innerText = "Parar Pesquisa ■";
                if(!searchStatus){
                    searchStatus = true;
                    document.querySelector("#form input[type=button]").click();
                    clicador = setInterval(function(){
                        document.querySelector("#form input[type=button]").click();
                    },3000);
                }else{
                    this.classList.remove('w3-pale-red');
                    this.classList.remove('w3-hover-red');
                    this.classList.add('w3-green');
                    this.classList.add('w3-hover-teal');
                    this.innerText = "Auto Pesquisar ►";
                    searchStatus = false;
                    clearInterval(clicador);
                }
            });
            shadow.appendChild(W3CSS);
            shadow.appendChild(wrapper);
            wrapper.appendChild(buttonAutoSearch);
        }
    }

    customElements.define('ajudador-pessoal', ajudadorPessoal);
    let shadow = document.createElement('ajudador-pessoal');
    shadow.style.position = "relative";
    shadow.style.zIndex = "9999";
    shadow.style.fontFamily = "calibri";
    document.querySelector("body").appendChild(shadow);
})();